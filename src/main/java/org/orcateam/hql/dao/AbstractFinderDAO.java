package org.orcateam.hql.dao;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.orcateam.hql.finder.*;
import org.slf4j.Logger;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;

public abstract class AbstractFinderDAO<E, PK extends Serializable> {

    Random random = new Random();

    /**
     * Spring session suplier method should be implemented
     *
     * @return
     */
    public abstract Session getSession();

    /**
     * Logger should be implemented
     *
     * @return
     */
    public abstract Logger getLogger();


    public Pagination<E> findWithPagination(Pager pager, Object param) {
        return findWithPagination(pager, param, getSession());
    }

    /**
     * Main pagination method.
     * Param will be used to generate Criteria to fetch data
     *
     * @param pager
     * @param param
     * @return
     */
    public Pagination<E> findWithPagination(Pager pager, Object param, Session session) {
        try {
            Class<?> obj = param.getClass();

            String clazzName = null;

            SelectAlias selectAlias = null;

            if (obj.isAnnotationPresent(ClazzInfo.class)) {
                ClazzInfo annotation = obj.getAnnotation(ClazzInfo.class);
                clazzName = annotation.clazz().getName().substring(annotation.clazz().getName().lastIndexOf(".") + 1);
                selectAlias = new SelectAlias(clazzName, annotation.alias());

            }

            List<String> whereClauses = new ArrayList<String>();
            Map<String, Object> values = new HashMap<String, Object>();
            List<JoinTemplateWrapper> joins = new ArrayList<JoinTemplateWrapper>();

            for (Field field : obj.getDeclaredFields()) {
                if (field.isAnnotationPresent(FieldInfo.class)) {
                    FieldInfo annotation = field.getAnnotation(FieldInfo.class);

                    FieldInfoReturnee processFieldInfo = processFieldInfo(annotation, field, param, selectAlias);
                    if (processFieldInfo != null) {
                        values.putAll(processFieldInfo.getValues());
                        if (StringUtils.isNotEmpty(processFieldInfo.getClause())) {
                            whereClauses.add(processFieldInfo.getClause());
                        }

                        if (processFieldInfo.getClauses().size() > 0) {
                            whereClauses.addAll(processFieldInfo.getClauses());
                        }
                        joins.addAll(processFieldInfo.getJoins());
                    }
                }

                if (field.isAnnotationPresent(Elements.class)) {
                    processElements(whereClauses, field, param);
                }

                if (field.isAnnotationPresent(Disjunction.class)) {
                    Disjunction annotation = field.getAnnotation(Disjunction.class);
                    List<FieldInfoReturnee> orList = new ArrayList<FieldInfoReturnee>();

                    for (FieldInfo fieldInfo : annotation.value()) {
                        FieldInfoReturnee fieldInfoReturnee = processFieldInfo(fieldInfo, field, param, selectAlias);
                        if (fieldInfoReturnee != null) {
                            orList.add(fieldInfoReturnee);
                        }

                    }

                    if (orList.size() > 0) {
                        String clause = " ( ";

                        int i = 0;
                        for (FieldInfoReturnee returnee : orList) {
                            i++;
                            clause = clause + returnee.getClause();

                            if (orList.size() != i) {
                                clause = clause + " OR ";
                            }
                            values.putAll(returnee.getValues());
                        }

                        clause = clause + " ) ";

                        whereClauses.add(clause);
                    }
                }


            }

            List<SelectAlias> moreSelects = new ArrayList<SelectAlias>();
            if (obj.isAnnotationPresent(SelectMores.class)) {
                SelectMores annotation = obj.getAnnotation(SelectMores.class);
                for (SelectMore selectMore : annotation.value()) {
                    clazzName = selectMore.clazzName();
                    moreSelects.add(new SelectAlias(clazzName, selectMore.alias()));
                }
            }

            if (obj.isAnnotationPresent(SelectMore.class)) {
                SelectMore annotation = obj.getAnnotation(SelectMore.class);
                clazzName = annotation.clazzName();
                moreSelects.add(new SelectAlias(clazzName, annotation.alias()));
            }

            StringBuffer selectBuilder = new StringBuffer();

            select(selectAlias, moreSelects, selectBuilder);
            join(obj, selectBuilder, true, selectAlias.getAlias(), joins);
            where(whereClauses, selectBuilder);
            order(pager, selectBuilder, selectAlias.getAlias());

            StringBuffer countBuilder = new StringBuffer();
            count(selectAlias, moreSelects, countBuilder);
            join(obj, countBuilder, false, selectAlias.getAlias(), joins);
            where(whereClauses, countBuilder);

            Query query = query(values, selectBuilder, session);
            Query countQuery = query(values, countBuilder, session);

            query.setFirstResult(pager.getFirstResult());
            query.setMaxResults(pager.getMaxResult());

            List<E> result = (List<E>) query.list();
            Long count = (Long) countQuery.uniqueResult();

            return new Pagination<E>(result, count.intValue());
        } catch (SecurityException e) {
            getLogger().error(e.getMessage(), e);
        } catch (IllegalArgumentException e) {
            getLogger().error(e.getMessage(), e);
        } catch (IllegalAccessException e) {
            getLogger().error(e.getMessage(), e);
        }

        return null;
    }

    private void processElements(List<String> whereClauses, Field field, Object param) throws IllegalAccessException {
        field.setAccessible(true);

        Elements annotation = field.getAnnotation(Elements.class);

        Object fieldValue = field.get(param);

        if (fieldValue != null) {
            String clause = annotation.alias() + " IN elements(" + annotation.inAliasField() + ") ";
            whereClauses.add(clause);
        }
    }


    private FieldInfoReturnee processFieldInfo(FieldInfo annotation, Field field, Object param, SelectAlias selectAlias) throws IllegalAccessException {
        field.setAccessible(true);
        String fieldName = field.getName();

        if (!annotation.fieldName().equals("")) {
            fieldName = annotation.fieldName();
        }

        Object fieldValue = field.get(param);

        CompareMethod compareMethod = annotation.compareMethod();
        boolean canNull = annotation.canNull();
        boolean nullCheckAnnotated = CompareMethod.IS_NULL == compareMethod || CompareMethod.NOT_NULL == compareMethod;

        String alias = selectAlias.getAlias();
        if (StringUtils.isNotEmpty(annotation.alias())) {
            alias = annotation.alias();
        }

        MatchMode matchMode = annotation.matchMode();

        boolean isMap = fieldValue != null && fieldValue instanceof Map;
        boolean isCollection = fieldValue != null && fieldValue instanceof Collection;
        boolean isString = fieldValue != null && fieldValue instanceof String;

        int mapSize = 0;
        int collectionSize = 0;
        int stringSize = 0;

        if (fieldValue != null) {
            if (isMap) {
                mapSize = ((Map) fieldValue).size();
            }
            if (isCollection) {
                collectionSize = ((Collection) fieldValue).size();
            }
            if (isString) {
                fieldValue = StringUtils.trim(fieldValue.toString());
                stringSize = ((String) fieldValue).length();
            }
        }

        if ((fieldValue != null || canNull || nullCheckAnnotated)
                && !(isString && stringSize == 0)
                && !(isMap && mapSize == 0)
                && !(isCollection && collectionSize == 0)) {
            FieldInfoReturnee returnee = new FieldInfoReturnee();
            if (isMap) {
                MapField mapAnnotation = field.getAnnotation(MapField.class);
                String mapKeyProp = mapAnnotation.keyProperty();
                String mapValueProp = mapAnnotation.valueProperty();
                JoinTemplate joinTemplate = mapAnnotation.joinTemplate();

                int rand = random.nextInt(100);
                rand = rand + 50;

                Map<String, String> map = (Map<String, String>) fieldValue;
                if (map.size() > 0) {
                    for (Map.Entry<String, String> attribute : map.entrySet()) {
                        String randomId = "" + rand;
                        rand++;

                        //creating an alias for every other map entry
                        String aliasMap = alias + randomId;

                        String paramNameKey = alias.concat("_").concat(mapKeyProp).concat("_").concat(randomId);
                        String paramNameVal = alias.concat("_").concat(mapValueProp).concat("_").concat(randomId);

                        JoinTemplateWrapper joinTemplateWrapper = new JoinTemplateWrapper();
                        joinTemplateWrapper.setAlias(aliasMap);
                        joinTemplateWrapper.setCollection(joinTemplate.collection());
                        joinTemplateWrapper.setJoinType(joinTemplate.joinType());
                        returnee.getJoins().add(joinTemplateWrapper);

                        String clause = aliasMap + "." + mapKeyProp + "= :" + paramNameKey + " AND ";

                        returnee.getValues().put(paramNameKey, attribute.getKey());
                        returnee.getValues().put(paramNameVal, attribute.getValue());

                        if (compareMethod == CompareMethod.LIKE) {
                            returnee.getValues().put(paramNameKey, attribute.getKey());
                            returnee.getValues().put(paramNameVal, matchMode.toMatchString(attribute.getValue()));
                            clause += toLower(aliasMap + "." + mapValueProp, paramNameVal);
                        } else if (compareMethod == CompareMethod.EQ) {
                            if (StringUtils.isBlank(mapValueProp)) {
                                clause += aliasMap + " = :" + paramNameVal;
                            } else {
                                clause += aliasMap + "." + mapValueProp + " = :" + paramNameVal;
                            }

                        } else {
                            throw new RuntimeException("Not implemented.");
                        }
                        returnee.getClauses().add(clause);
                    }
                }
            } else {
                String paramName = alias.concat(field.getName());

                paramName = StringUtils.replace(paramName, ".", "");
                String clause = "";

                if (canNull && fieldValue == null) {
                    clause = alias + "." + fieldName + " IS NULL";
                } else {
                    returnee.getValues().put(paramName, fieldValue);

                    if (compareMethod == CompareMethod.EQ) {
                        clause = alias + "." + fieldName + " = :" + paramName;

                    } else if (compareMethod == CompareMethod.NE) {
                        clause = alias + "." + fieldName + " != :" + paramName;

                    } else if (compareMethod == CompareMethod.GT) {
                        clause = alias + "." + fieldName + " > :" + paramName;

                    } else if (compareMethod == CompareMethod.GE) {
                        clause = alias + "." + fieldName + " >= :" + paramName;

                    } else if (compareMethod == CompareMethod.LT) {
                        clause = alias + "." + fieldName + " < :" + paramName;

                    } else if (compareMethod == CompareMethod.LE) {
                        clause = alias + "." + fieldName + " <= :" + paramName;

                    } else if (compareMethod == CompareMethod.IN) {
                        clause = alias + "." + fieldName + " IN (:" + paramName + ")";

                    } else if (compareMethod == CompareMethod.NOT_IN) {
                        clause = alias + "." + fieldName + " NOT IN (:" + paramName + ")";

                    } else if (compareMethod == CompareMethod.IN_COLLECTION) {
                        String clause1 = annotation.inAlias() + "." + fieldName + " = :" + paramName;
                        String clause2 = alias + " IN elements(" + annotation.inAlias() + "." + annotation.inAliasField() + ") ";
                        clause = " (" + clause1 + " AND " + clause2 + ") ";

                    } else if (compareMethod == CompareMethod.LIKE) {
                        //put value again for "like search"
                        returnee.getValues().put(paramName, matchMode.toMatchString(fieldValue.toString()));
                        clause = toLower(alias + "." + fieldName, paramName);

                    } else if (compareMethod == CompareMethod.IS_NULL) {
                        clause = alias + "." + fieldName + " IS NULL";
                        returnee.getValues().remove(paramName);
                    } else if (compareMethod == CompareMethod.NOT_NULL) {
                        clause = alias + "." + fieldName + " IS NOT NULL";
                        returnee.getValues().remove(paramName);
                    }
                }

                returnee.setClause(clause);
            }

            return returnee;
        }

        return null;
    }

    private Query query(Map<String, Object> values, StringBuffer queryBuilder, Session session) {
        Query query = session.createQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : values.entrySet()) {
            if (entry.getValue() instanceof Collection) {
                query.setParameterList(entry.getKey(), (Collection) entry.getValue());
            } else {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query;
    }

    private void order(Pager pager, StringBuffer queryBuilder, String selectAlias) {
        queryBuilder.append(" ORDER BY ");

        OrderBy orderBy = pager.getOrderBy();
        //queryBuilder.append(selectAlias);

        if (!StringUtils.isEmpty(orderBy.getName())) {

            queryBuilder.append(orderBy.getName());

            if (orderBy.isAsc()) {
                queryBuilder.append(" ASC");
            } else {
                queryBuilder.append(" DESC");
            }
            if (!orderBy.getName().equals(selectAlias + ".id")) {
                queryBuilder.append(",").append(selectAlias).append(".id");
            }
        } else {
            queryBuilder.append(selectAlias).append(".id");

            if (orderBy.isAsc()) {
                queryBuilder.append(" ASC");
            } else {
                queryBuilder.append(" DESC");
            }
        }

    }

    private void select(SelectAlias selectAlias, List<SelectAlias> moreSelects, StringBuffer queryBuilder) {
        queryBuilder.append("SELECT distinct ")
                .append(selectAlias.getAlias());

        queryBuilder.append(" FROM ")
                .append(selectAlias.getClazz()).append(" ")
                .append(selectAlias.getAlias());


        for (SelectAlias a : moreSelects) {
            queryBuilder.append(" , ")
                    .append(a.getClazz()).append(" ")
                    .append(a.getAlias());
        }
    }

    private void count(SelectAlias selectAlias, List<SelectAlias> moreSelects, StringBuffer queryBuilder) {
        queryBuilder.append("SELECT count( distinct ")
                .append(selectAlias.getAlias())
                .append(")");

        queryBuilder.append(" FROM ")
                .append(selectAlias.getClazz()).append(" ")
                .append(selectAlias.getAlias());


        for (SelectAlias a : moreSelects) {
            queryBuilder.append(" , ")
                    .append(a.getClazz()).append(" ")
                    .append(a.getAlias());
        }
    }


    private void join(Class<?> obj, StringBuffer queryBuilder, boolean fetch, String selectAlias, List<JoinTemplateWrapper> joins) {
        if (obj.isAnnotationPresent(Join.class)) {
            Join annotation = obj.getAnnotation(Join.class);
            joinAnnotation(queryBuilder, selectAlias, annotation, fetch);
        }

        if (obj.isAnnotationPresent(Joins.class)) {
            Joins annotation = obj.getAnnotation(Joins.class);
            for (Join join : annotation.value()) {
                joinAnnotation(queryBuilder, selectAlias, join, fetch);
            }
        }

        for (JoinTemplateWrapper join : joins) {
            joinTemplate(queryBuilder, selectAlias, join, fetch);
            ;
        }
    }

    private void where(List<String> whereClauses, StringBuffer queryBuilder) {
        if (whereClauses.size() > 0) {
            int i = 0;
            queryBuilder.append(" WHERE ");
            for (String s : whereClauses) {
                i++;
                queryBuilder.append(s);
                if (whereClauses.size() != i) {
                    queryBuilder.append(" AND ");
                }
            }
        }
    }

    private void joinAnnotation(StringBuffer queryBuilder, String selectAlias, Join annotation, boolean fetch) {
        JoinType joinType = annotation.joinType();
        String collection = annotation.collection();
        if (joinType == JoinType.LEFT_OUTER_JOIN) {
            queryBuilder.append(" LEFT JOIN ");
            if (fetch) {
                queryBuilder.append(" FETCH ");
            }
            queryBuilder.append(" ").append(collection).append(" AS ").append(annotation.alias());
        } else {
            throw new RuntimeException("Not implemented yet");
        }
    }

    private void joinTemplate(StringBuffer queryBuilder, String selectAlias, JoinTemplateWrapper annotation, boolean fetch) {
        JoinType joinType = annotation.getJoinType();
        String collection = annotation.getCollection();
        if (joinType == JoinType.LEFT_OUTER_JOIN) {
            queryBuilder.append(" LEFT JOIN ");
            if (fetch) {
                queryBuilder.append(" FETCH ");
            }
            queryBuilder.append(" ").append(collection).append(" AS ").append(annotation.getAlias());
        } else {
            throw new RuntimeException("Not implemented yet");
        }
    }


    public String toLower(String field, String param) {
        return "NLS_LOWER (" + field + ") like NLS_LOWER (:" + param + ")";
    }

    class SelectAlias {

        private String clazz;
        private String alias;

        public SelectAlias(String clazz, String alias) {
            this.clazz = clazz;
            this.alias = alias;
        }

        public String getClazz() {
            return clazz;
        }

        public void setClazz(String clazz) {
            this.clazz = clazz;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

    }


    class FieldInfoReturnee {

        private String clause;
        private Map<String, Object> values = new HashMap<String, Object>();
        private List<JoinTemplateWrapper> joins = new ArrayList<JoinTemplateWrapper>();
        private List<String> clauses = new ArrayList<String>();

        public String getClause() {
            return clause;
        }

        public void setClause(String clause) {
            this.clause = clause;
        }

        public Map<String, Object> getValues() {
            return values;
        }

        public void setValues(Map<String, Object> values) {
            this.values = values;
        }

        public List<String> getClauses() {
            return clauses;
        }

        public void setClauses(List<String> clauses) {
            this.clauses = clauses;
        }

        public List<JoinTemplateWrapper> getJoins() {
            return joins;
        }

        public void setJoins(List<JoinTemplateWrapper> joins) {
            this.joins = joins;
        }

    }

    class JoinTemplateWrapper {
        private String collection;

        private String alias;

        private JoinType joinType;

        public String getCollection() {
            return collection;
        }

        public void setCollection(String collection) {
            this.collection = collection;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public JoinType getJoinType() {
            return joinType;
        }

        public void setJoinType(JoinType joinType) {
            this.joinType = joinType;
        }
    }
}
