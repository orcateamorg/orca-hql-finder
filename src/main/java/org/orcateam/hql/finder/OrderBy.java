package org.orcateam.hql.finder;

import java.io.Serializable;

public class OrderBy implements Serializable {

    private String name;
    private boolean asc = true;

    public OrderBy() {
    }

    public OrderBy(String name) {
        super();
        this.name = name;
    }

    public OrderBy(String name, boolean asc) {
        this.name = name;
        this.asc = asc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }
}
