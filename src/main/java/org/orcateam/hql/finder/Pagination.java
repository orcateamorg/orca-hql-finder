package org.orcateam.hql.finder;

import java.util.List;

public class Pagination<E> {

    private List<E> result;
    private Integer count;

    public Pagination() {
    }

    public Pagination(List<E> result, int count) {
        this.result = result;
        this.count = count;
    }

    public List<E> getResult() {
        return result;
    }

    public void setResult(List<E> result) {
        this.result = result;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
