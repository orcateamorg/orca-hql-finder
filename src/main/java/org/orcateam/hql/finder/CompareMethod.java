package org.orcateam.hql.finder;

public enum CompareMethod {
    EQ, NE, GT, GE, LT, LE, IN, NOT_IN, LIKE, IN_COLLECTION, IS_NULL, NOT_NULL
}
